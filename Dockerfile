FROM registry.gitlab.com/logius/beveiligd-koppelvlak-extern/base-images/eclipse-temurin:17-jre-alpine-opentelemetry

USER root

# Create user to comply to numeric UID for k8s. This workaround should be adopted in the base image.
RUN addgroup cpaservice && adduser -S -h /home/cpaservice -G cpaservice -u 1001 cpaservice && \
  chown -R cpaservice:cpaservice /opt/app && \
  chown -h cpaservice:cpaservice /home/cpaservice

USER 1001

COPY --chown=cpaservice:cpaservice target/*.jar /opt/app/app.jar

EXPOSE 8080 5701
ENTRYPOINT ["java","-javaagent:opentelemetry-javaagent.jar","-jar","/opt/app/app.jar"]
