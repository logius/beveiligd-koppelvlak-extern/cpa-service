# CPA-service & OSDBK

The CPA Service holds the CPAs for the OSDBK stack and has its own CPA database. The CPA Service can be queried by the CPA Admin Console to 
query and manage the CPA database.It also serves as the link between the JMS Consumer and Ebms Core, by providing a matching CPA for a
received JMS message.

The CPA Service is part of OSDBK (Open Source Dienst Beveiligd Koppelvlak). OSDBK consists of seven applications:

- EbMS Core / EbMS Admin
- JMS Producer
- JMS Consumer
- Apache ActiveMQ
- CPA Service
- OSDBK Admin Console
- Throttling Service (optional)

The CPA Service as of version 1.6.0 still needs to synchronize the CPA database with the Ebms Core CPA database, so that the latter is a mirror
of the former. In future versions, Ebms Core will use the CPA database as its source.

### CPA Database Configuration

The database connection can be configured with the following parameters:
~~~
spring:
  datasource:
    url: jdbc:postgresql://cpa-database:5432/cpa
    username: cpa
    password: cpa
~~~

### ActiveMQ JMX Configuration

The CPA Service uses the ActiveMQ Management Console to pause and resume queues. Usually, when changing a CPA for an Afnemer,
all message processing needs to be paused. When the new CPA has been loaded into the database, message processing can be resumed.

The `broker-name` needs to correspond with the unique name of the ActiveMQ Broker. This name is defined is the `activemq.xml` configuration file,
and visible in the ActiveMQ Console Homepage. When using the OSDBK ActiveMQ, the broker-name is `activemq-ebms-docker`, and the credentials are
`admin` / `admin`.

~~~
spring:
  activemq:
    jmx-url: "service:jmx:rmi:///jndi/rmi://activemq:10051/jmxrmi"
    broker-name: activemq-ebms-docker
    username: admin
    password: admin
~~~

### Ebms Core CPA Service

The CPA Service as of version 1.6.0 still needs to synchronize the CPA database with the Ebms Core CPA database. For this, the url 
to the Ebms Core CPA Service needs to be defined.

~~~
ebms:
  cpa-service:
    url: "http://ebms-core:8888/service/rest/v19/cpas"
~~~

### REST endpoints
The CPA Service exposes one endpoint to the JMS Consumer, so that it can find a matching CPA for an incoming JMS message.
~~~
http://cpa-service:8080/cpas/find
~~~

The CPA Service features many endpoints that are used by the CPA Admin Console to manage the CPA database. These endpoints are protected 
by basic authentication and not exposed to other applications.Basic Authentication can be configured via:
~~~
spring:
  security:
    user:
      name: changeit
      password: changeit
~~~

## Examples

#### Example of Pinging parties
~~~
curl --location --request POST 'localhost:8084/cpa/DGL-VERWERKEN-1-0$1-0_00000004003214345001-123456789012345678900001_928792FC79F211E8B020005056810F3E/ping' \
--header 'Authorization: Basic Y2hhbmdlaXQ6Y2hhbmdlaXQ='
~~~

### How to run this application locally
An example docker-compose has been provided. This can be run to test this application.
However, the following variables need to be populated by means of a `.env` file:

~~~
${OSDBK_IMAGE_REPOSITORY} -- location of your Docker repository where you are housing your built OSDBK docker images
${ACTIVEMQ_IMAGE_TAG} -- the tag of your built ActiveMQ docker image
${ACTIVEMQ_POSTGRESQL_USERNAME} -- the username used to connect to the ActiveMQ database
${ACTIVEMQ_POSTGRESQL_PASSWORD} -- the password used to connect to the ActiveMQ database
${DGL_STUB_IMAGE_TAG}  -- the tag of your built stub application docker image, to simulate interactions with an external party
${DGL_STUB_MESSAGE_SENDER_OIN_AFNEMER} -- the OIN of your stubbed application
${EBMS_JDBC_USERNAME} -- the username used to connect to the EBMS database
${EBMS_JDBC_PASSWORD} -- the password used to connect to the EBMS database
${EBMS_JMS_BROKER_USERNAME} -- the username used to connect the ebms application to the ActiveMQ instance
${EBMS_JMS_BROKER_PASSWORD} -- the password used to connect the ebms application to the ActiveMQ instance
${EBMS_POSTGRESQL_IMAGE_TAG} -- the tag of your built EBMS PostgresQL docker image
${EBMS_STUB_IMAGE_TAG} -- the tag of your built stub application docker image, to simulate interactions with an external party
${JMS_CONSUMER_IMAGE_TAG} -- the tag of your built jms-consumer docker image
${JMS_CONSUMER_ACTIVEMQ_USER} -- the username used to connect the jms-consumer application to the ActiveMQ instance
${JMS_CONSUMER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-consumer application to the ActiveMQ instance
${JMS_PRODUCER_IMAGE_TAG} -- the tag of your built jms-producer docker image
${JMS_PRODUCER_ACTIVEMQ_USER} -- the username used to connect the jms-producer application to the ActiveMQ instance
${JMS_PRODUCER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-producer application to the ActiveMQ instance
${THROTTLING_SERVICE_IMAGE_TAG} -- the tag of your built throttling docker image
${CPA_SERVICE_IMAGE_TAG} -- the tag of your built cpa-service docker image
${OSDBK_ADMIN_CONSOLE_IMAGE_TAG} -- the tag of your built OSDBK Admin console docker image
${CPA_JDBC_USERNAME} -- the username used to connect to the CPA database
${CPA_JDBC_PASSWORD} -- the password used to connect to the CPA database
${CPA_SERVICE_ACTIVEMQ_USER} -- the username used to connect the cpa-service application to the ActiveMQ instance
${CPA_SERVICE_ACTIVEMQ_PASSWORD} -- the password used to connect the cpa-service application to the ActiveMQ instance
${CPA_DATABASE_IMAGE_TAG} -- the tag of your built CPA PostgresQL docker image
~~~

Example command to bring up the application:
~~~
docker-compose --env-file path/to/env/file/.env up
~~~

## Release notes
See [NOTES][NOTES] for latest.

[NOTES]: templates/NOTES.txt

### Older release notes collated below:

V Up to 1.12.3
- Various improvements to:
    - container technology
    - use of base-images
    - GITLAB Security scan framework implemented
    - Improved Open Source build process and container releases
    - Test improvements via docker-compose
    - Dependency upgrades
