package nl.logius.osdbk.cache;

import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class CacheConfig extends CachingConfigurerSupport {

    @Override
    @Bean("cpaKeyGenerator")
    public KeyGenerator keyGenerator() {
        return new CpaKeyGenerator();
    }
}
