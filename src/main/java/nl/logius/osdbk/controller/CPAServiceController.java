package nl.logius.osdbk.controller;

import lombok.RequiredArgsConstructor;
import nl.logius.osdbk.cpa.model.CPAParty;
import nl.logius.osdbk.cpa.orchestrator.CPAAndEbmsCoreRestOrchestrator;
import nl.logius.osdbk.cpa.service.CPAService;
import nl.logius.osdbk.jmx.ActiveMQJmxService;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class CPAServiceController {

    private final CPAAndEbmsCoreRestOrchestrator cpaAndEbmsCoreRestOrchestrator;
    private final CPAService cpaService;
    private final ActiveMQJmxService activeMQJmxService;

    @PutMapping("/cpa/validate")
    public void validateCPA(@RequestBody String cpa) {
        cpaService.validateCPA(cpa);
    }

    @PostMapping("/cpa/insert")
    public String insertCPA(@RequestBody String cpa) {
        return cpaService.insertCPA(cpa);
    }

    @GetMapping("/cpa")
    public List<String> getCPAIds() {
        return cpaService.getCPAIds();
    }

    @GetMapping("/cpa/active")
    public List<String> getActiveCPAIds() {
        return cpaService.getActiveCPAIds();
    }

    @GetMapping("/cpa/inactive")
    public List<String> getInactiveCPAIds() {
        return cpaService.getInactiveCPAIds();
    }

    @GetMapping("/cpa/sync")
    public void synchronizeCPAs() {
        cpaService.synchronizeCPAs();
    }

    @GetMapping("/cpa/{cpaId}")
    public String getCPA(@PathVariable String cpaId) {
        return cpaService.getCPA(cpaId);
    }

    /**
     * Pings the parties from CPA and returns the parties as part of result.
     *
     * @param cpaId         the ID of CPA document
     * @param fromPartyIdx  the Index of fromParty in CPA
     * @param toPartyIdx    the Index of toParty in CPA
     * @return a String containing formatted results: "fromPartyId=%s, toPartyId=%s" without prefixes
     */
    @PostMapping("/cpa/ping/{cpaId}/fromIdx/{fromPartyIdx}/toIdx/{toPartyIdx}")
    public String ping(@PathVariable String cpaId, @PathVariable int fromPartyIdx, @PathVariable int toPartyIdx) {
        return cpaAndEbmsCoreRestOrchestrator.ping(cpaId, fromPartyIdx, toPartyIdx);
    }

    @GetMapping("/cpa/{cpaId}/party")
    public List<CPAParty> getCPAParties(@PathVariable String cpaId) {
        return cpaService.getCPAParties(cpaId);
    }

    // used by JMS Consumer to match CPA
    @GetMapping("/cpa/find")
    public MultiValueMap<String, String> findCPA(@RequestParam MultiValueMap<String, String> requestParams) {
        return cpaService.findCPA(requestParams);
    }

    @DeleteMapping("/cpa/{cpaId}")
    public void deleteCPA(@PathVariable String cpaId) {
        cpaService.deleteCPA(cpaId);
    }

    @PutMapping("/cpa/activate/{cpaId}")
    public void activateCPA(@PathVariable String cpaId) {
        cpaService.activateCPA(cpaId);
    }

    @PutMapping("/cpa/deactivate/{cpaId}")
    public void deactivateCPA(@PathVariable String cpaId) {
        cpaService.deactivateCPA(cpaId);
    }
    
    @PutMapping("/cpa/resend/failed/{cpaId}")
    public void resendFailedMessagesForCPA(@PathVariable String cpaId) {
        cpaService.resendFailed(cpaId);
    }

    @PutMapping("/cpa/resend/expired/{cpaId}")
    public void resendExpiredMessagesForCPA(@PathVariable String cpaId) {
        cpaService.resendExpired(cpaId);
    }

    @PutMapping("/jmx/{queueName}")
    public void toggleQueueStatus(@PathVariable String queueName, @RequestParam String action) {
        activeMQJmxService.toggleQueueStatus(queueName, action);
    }
}
