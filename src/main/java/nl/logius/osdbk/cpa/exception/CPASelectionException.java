package nl.logius.osdbk.cpa.exception;

public class CPASelectionException extends RuntimeException {

    public CPASelectionException(String message) {
        super(message);
    }

}
