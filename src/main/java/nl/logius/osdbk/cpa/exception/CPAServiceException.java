package nl.logius.osdbk.cpa.exception;

public class CPAServiceException extends RuntimeException {

    public CPAServiceException(String errorMessage, Throwable t) {
        super(t);
    }

    public CPAServiceException(String message) {
        super(message);
    }
}
