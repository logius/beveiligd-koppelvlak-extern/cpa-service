package nl.logius.osdbk.cpa.model;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "CPA")
public class CPA implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private long id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPA_ID", unique = true)
    private String cpaId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CPA_CONTENT", columnDefinition = "TEXT")
    private String cpaContent;

    @Basic(optional = false)
    @NotNull
    @Column(name = "VERSION")
    private String version;

    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "INSERT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IS_ACTIVE")
    private boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getCpaId() {
        return cpaId;
    }

    public void setCpaId(String cpaId) {
        this.cpaId = cpaId;
    }

    public String getCpaContent() {
        return cpaContent;
    }

    public void setCpaContent(String cpaContent) {
        this.cpaContent = cpaContent;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getStartDate() {
        if (startDate != null) {
            return new Date(startDate.getTime());
        }
        return null;
    }

    public void setStartDate(Date startDate) {
        this.startDate = (Date) startDate.clone();
    }

    public Date getEndDate() {
        if (endDate != null) {
            return new Date(endDate.getTime());
        }
        return null;
    }

    public void setEndDate(Date endDate) {
        this.endDate = (Date) endDate.clone();
    }

    public Date getInsertDate() {
        if (insertDate != null) {
            return new Date(insertDate.getTime());
        }
        return null;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = (Date) insertDate.clone();
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "nl.logius.osdbk.cpa.Cpa[ cpaId=" + cpaId + " ]";
    }

}
