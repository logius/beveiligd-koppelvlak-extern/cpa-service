package nl.logius.osdbk.cpa.model;

import java.util.List;

public class CPAParty {

    private String partyId;
    private String partyName;
    private List<CPARole> cpaRoles;

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public List<CPARole> getCpaRoles() {
        return cpaRoles;
    }

    public void setCpaRoles(List<CPARole> cpaRoles) {
        this.cpaRoles = cpaRoles;
    }


}
