package nl.logius.osdbk.cpa.model;

import java.util.List;

public class CPARole {

    private String name;
    private String service;
    private List<String> receiveActions;
    private List<String> sendActions;

    public String getName() {
        return name;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getReceiveActions() {
        return receiveActions;
    }

    public void setReceiveActions(List<String> receiveActions) {
        this.receiveActions = receiveActions;
    }

    public List<String> getSendActions() {
        return sendActions;
    }

    public void setSendActions(List<String> sendActions) {
        this.sendActions = sendActions;
    }
    
    

}
