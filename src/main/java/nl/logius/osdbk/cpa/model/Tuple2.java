package nl.logius.osdbk.cpa.model;

public record Tuple2<K, V>(K first, V second) {}