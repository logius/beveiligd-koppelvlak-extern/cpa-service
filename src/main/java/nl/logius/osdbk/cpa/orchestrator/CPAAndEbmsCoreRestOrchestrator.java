package nl.logius.osdbk.cpa.orchestrator;

import lombok.RequiredArgsConstructor;
import nl.logius.osdbk.cpa.exception.CPAServiceException;
import nl.logius.osdbk.cpa.model.Tuple2;
import nl.logius.osdbk.cpa.service.CPAService;
import nl.logius.osdbk.rest.EbmsCoreRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CPAAndEbmsCoreRestOrchestrator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CPAAndEbmsCoreRestOrchestrator.class);
    private static final String URN_OSB_OIN_PARTY_ID_PREFIX = "urn:osb:oin:";

    private final CPAService cpaService;
    private final EbmsCoreRestService ebmsCoreRestService;

    /**
     * Pings the parties from CPA and returns the parties as part of result.
     *
     * @param cpaId         the ID of CPA document
     * @param fromPartyIdx  the Index of fromParty in CPA
     * @param toPartyIdx    the Index of toParty in CPA
     * @return a String containing formatted results: "fromPartyId=%s, toPartyId=%s" without prefixes
     */
    public String ping(String cpaId, int fromPartyIdx, int toPartyIdx) {
        Tuple2<String, String> partyIds = validateAndGetPartyIds(cpaId, fromPartyIdx, toPartyIdx);
        var fromPartyIdWithoutPrefix = partyIds.first();
        var toPartyIdWithoutPrefix = partyIds.second();
        var fromPartyId = URN_OSB_OIN_PARTY_ID_PREFIX.concat(fromPartyIdWithoutPrefix);
        var toPartyId = URN_OSB_OIN_PARTY_ID_PREFIX.concat(toPartyIdWithoutPrefix);
        LOGGER.info("Will ping CPA with id={}, fromPartyId={}, toPartyId={}", cpaId, fromPartyId, toPartyId);
        ebmsCoreRestService.ping(cpaId, fromPartyId, toPartyId);
        LOGGER.info("Pinged CPA with id={}, fromPartyId={}, toPartyId={}", cpaId, fromPartyId, toPartyId);
        return "fromPartyId=%s, toPartyId=%s".formatted(fromPartyIdWithoutPrefix, toPartyIdWithoutPrefix);
    }

    Tuple2<String, String> validateAndGetPartyIds(String cpaId, int fromPartyIdx, int toPartyIdx) {
        LOGGER.info("Validating ping and getting parties from CPA with id={}, fromPartyIdx={}, toPartyIdx={}", cpaId, fromPartyIdx, toPartyIdx);
        if (fromPartyIdx == toPartyIdx) {
            throw new CPAServiceException("FromPartyIdx and ToPartyIdx cannot be the same");
        }
        var cpaParties = cpaService.getCPAParties(cpaId);
        var fromPartyIdWithoutPrefix = cpaParties.get(fromPartyIdx).getPartyId();
        var toPartyIdWithoutPrefix = cpaParties.get(toPartyIdx).getPartyId();
        return new Tuple2<>(fromPartyIdWithoutPrefix, toPartyIdWithoutPrefix);
    }
}
