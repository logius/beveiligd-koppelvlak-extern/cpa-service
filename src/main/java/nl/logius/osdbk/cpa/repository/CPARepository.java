package nl.logius.osdbk.cpa.repository;

import java.util.List;
import java.util.Optional;

import nl.logius.osdbk.cpa.model.CPA;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CPARepository extends JpaRepository<CPA, Long> {

    @Query("SELECT c FROM CPA c WHERE c.isActive = true")
    List<CPA> findActiveCpas();

    @Query("SELECT c.cpaId FROM CPA c WHERE c.isActive = true")
    List<String> findActiveCpaIds();

    @Query("SELECT c.cpaId FROM CPA c WHERE c.isActive = false")
    List<String> findInactiveCpaIds();

    @Query("SELECT c.cpaId FROM CPA c")
    List<String> findCpaIds();
    
    Optional<CPA> findByCpaId(String cpaId);

    void deleteByCpaId(String cpaId);
    
    @Modifying
    @Query("UPDATE CPA c SET c.isActive = true WHERE c.cpaId = :cpaId")
    void setActive(@Param("cpaId") String cpaId);
    
    @Modifying
    @Query("UPDATE CPA c SET c.isActive = false WHERE c.cpaId = :cpaId")
    void setInActive(@Param("cpaId") String cpaId);

}
