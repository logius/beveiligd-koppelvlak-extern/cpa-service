package nl.logius.osdbk.cpa.service;

import lombok.RequiredArgsConstructor;
import nl.logius.osdbk.cpa.exception.CPASelectionException;
import nl.logius.osdbk.cpa.repository.CPARepository;
import nl.logius.osdbk.cpa.util.CPAParser;
import nl.logius.osdbk.cpa.util.CPASearchCriteria;
import org.apache.commons.lang3.StringUtils;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;
import org.oasis.open.ebxml.cpa.CollaborationRole;
import org.oasis.open.ebxml.cpa.PartyInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class CPASelectionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CPASelectionService.class);

    private final CPARepository cpaRepository;

    public MultiValueMap<String, String> findCPA(CPASearchCriteria searchCriteria) {
        List<CollaborationProtocolAgreement> cpas = loadCPAs();
        cpas = cpas.stream()
                .filter(cpa -> existsPartyId(cpa, searchCriteria.getSender()) && existsPartyId(cpa, searchCriteria.getReceiver()))
                .filter(cpa -> isValidParty(cpa, searchCriteria.getSender(), searchCriteria, true))
                .filter(cpa -> isValidParty(cpa, searchCriteria.getReceiver(), searchCriteria, false))
                .collect(Collectors.toList());

        CollaborationProtocolAgreement cpa = handleSearchResult(cpas, searchCriteria);

        String service = getService(cpa, searchCriteria);
        String fromPartyName = findPartyName(cpa, searchCriteria.getSender());
        String toPartyName = findPartyName(cpa, searchCriteria.getReceiver());

        MultiValueMap<String, String> responseParams = new LinkedMultiValueMap<>();
        responseParams.add("cpaId", cpa.getCpaid());
        responseParams.add("service", service);
        responseParams.add("fromPartyName", fromPartyName);
        responseParams.add("toPartyName", toPartyName);
        return responseParams;
    }

    @Cacheable(cacheNames = "CPA_CACHE", keyGenerator = "cpaKeyGenerator")
    public List<CollaborationProtocolAgreement> loadCPAs() {
        List<CollaborationProtocolAgreement> cpas = cpaRepository.findActiveCpas().stream()
                .map(cpa -> CPAParser.parseToCollaborationProtocolAgreement(cpa.getCpaContent()))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        LOGGER.info("Loaded CPAs: {} ", cpas.stream().map(CollaborationProtocolAgreement::getCpaid).collect(Collectors.toList()));
        return cpas;
    }

    private boolean existsPartyId(CollaborationProtocolAgreement cpa, String partyId) {
        return cpa.getPartyInfo().stream()
                .flatMap(p -> p.getPartyId().stream())
                .anyMatch(id -> partyId.equalsIgnoreCase(id.getValue()));
    }

    private String getService(CollaborationProtocolAgreement cpa, CPASearchCriteria searchCriteria) {
        for (PartyInfo p : cpa.getPartyInfo()) {
            for (CollaborationRole cr : p.getCollaborationRole()) {
                String serviceFromCPA = cr.getServiceBinding().getService().getValue();
                if (serviceFromCPA.equals(searchCriteria.getService()) || serviceFromCPA.equals(searchCriteria.getService() + searchCriteria.getServiceExtension())) {
                    return serviceFromCPA;
                }
            }
        }
        throw new CPASelectionException("Error: No matching service found in CPA");
    }

    private CollaborationProtocolAgreement handleSearchResult(List<CollaborationProtocolAgreement> cpas, CPASearchCriteria searchCriteria) {

        if (cpas.isEmpty()) {
            throw new CPASelectionException("Error. No CPA found for sender=" + searchCriteria.getSender() + ", receiver="
                    + searchCriteria.getReceiver() + ", action=" + searchCriteria.getAction() + " and service=" + searchCriteria.getService());
        } else if (cpas.size() == 1) {
            LOGGER.info("Valid CPA found for sender {} and receiver {} : {} ",
                    searchCriteria.getSender(), searchCriteria.getReceiver(), cpas.get(0).getCpaid());
            return cpas.get(0);
        } else {
            StringBuilder foundCPAIds = new StringBuilder();
            cpas.forEach(cpa -> foundCPAIds.append(cpa.getCpaid()).append(", "));

            throw new CPASelectionException("Error. Multiple CPA's found for message with sender="
                    + searchCriteria.getSender() + ", receiver=" + searchCriteria.getReceiver() + ", action=" + searchCriteria.getAction()
                    + " and service=" + searchCriteria.getService() + ". CPAs: " + foundCPAIds);
        }
    }

    private boolean isValidParty(CollaborationProtocolAgreement cpa, String partyId, CPASearchCriteria searchCriteria, boolean isFromParty) {
        for (PartyInfo p : cpa.getPartyInfo()) {
            String cpaPartyId = p.getPartyId().get(0).getValue();

            if (StringUtils.equals(cpaPartyId, partyId) && checkServiceAndAction(p, searchCriteria, isFromParty)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkServiceAndAction(PartyInfo partyInfo, CPASearchCriteria searchCriteria, boolean isFromParty) {
        AtomicBoolean isValid = new AtomicBoolean(false);

        partyInfo.getCollaborationRole().forEach(cr -> {
            if (StringUtils.equals(cr.getServiceBinding().getService().getValue(), searchCriteria.getService())
                    || StringUtils.equals(cr.getServiceBinding().getService().getValue(), searchCriteria.getService() + searchCriteria.getServiceExtension())) {
                checkParties(isFromParty, partyInfo, searchCriteria, isValid);
            }
        });
        return isValid.get();
    }

    private void checkParties(boolean isFromParty, PartyInfo partyInfo, CPASearchCriteria searchCriteria, AtomicBoolean isValid) {
        if (isFromParty) {
            partyInfo.getCollaborationRole().forEach(cr -> cr.getServiceBinding().getCanSend().forEach(canSend -> {
                if (StringUtils.equals(canSend.getThisPartyActionBinding().getAction(), searchCriteria.getAction())) {
                    isValid.set(true);
                }
            }));

        } else {
            partyInfo.getCollaborationRole().forEach(cr -> cr.getServiceBinding().getCanReceive().forEach(canReceive -> {
                if (StringUtils.equals(canReceive.getThisPartyActionBinding().getAction(), searchCriteria.getAction())) {
                    isValid.set(true);
                }
            }));
        }
    }

    private String findPartyName(CollaborationProtocolAgreement cpa, String partyId) {
        for (PartyInfo p : cpa.getPartyInfo()) {
            String partyIdFromCPA = p.getPartyId().get(0).getValue();
            String roleName = p.getCollaborationRole().get(0).getRole().getName();
            if (partyId.equals(partyIdFromCPA)) {
                return roleName;
            }
        }
        throw new CPASelectionException("Error: No matching partyId found in CPA");
    }

}
