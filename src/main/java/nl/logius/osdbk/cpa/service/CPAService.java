package nl.logius.osdbk.cpa.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import nl.logius.osdbk.cpa.exception.CPAServiceException;
import nl.logius.osdbk.cpa.model.CPA;
import nl.logius.osdbk.cpa.model.CPAParty;
import nl.logius.osdbk.cpa.repository.CPARepository;
import nl.logius.osdbk.cpa.util.CPAParser;
import nl.logius.osdbk.cpa.util.CPASearchCriteria;
import nl.logius.osdbk.rest.EbmsCoreCpaService;
import nl.logius.osdbk.rest.JmsConsumerService;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CPAService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CPAService.class);

    private final CPARepository cpaRepository;
    private final CPASelectionService cpaSelectionService;
    private final EbmsCoreCpaService ebmsCoreCpaService;
    private final JmsConsumerService jmsConsumerService;

    public void validateCPA(String cpa) {
        ebmsCoreCpaService.validateCPA(cpa);
    }

    @Transactional
    @CacheEvict(cacheNames = "CPA_CACHE", allEntries = true)
    public String insertCPA(String cpaString) {
        CPA cpa = CPAParser.parseToCPAEntity(cpaString);
        try {
            ebmsCoreCpaService.insertCPA(cpaString); // necessary call until ebms core uses cpa-service-database
            cpaRepository.save(cpa);
            LOGGER.info("Inserted CPA with id {}", cpa.getCpaId());
        } catch (DataIntegrityViolationException ex) {
            throw new CPAServiceException(String.format("Can not insert CPA with id %s, since a CPA with this id already exists.", cpa.getCpaId()));
        }
        return cpa.getCpaId();
    }

    public List<String> getCPAIds() {
        return cpaRepository.findCpaIds();
    }

    public List<String> getActiveCPAIds() {
        return cpaRepository.findActiveCpaIds();
    }

    public List<String> getInactiveCPAIds() {
        return cpaRepository.findInactiveCpaIds();
    }

    @Transactional
    public void synchronizeCPAs() {
        List<String> cpaIdsFromEbmsCore = ebmsCoreCpaService.getCpaIds();
        List<String> cpaIdsFromCPAService = cpaRepository.findCpaIds();

        if (cpaIdsFromEbmsCore != null && cpaIdsFromCPAService != null) {
            for (String cpaId : cpaIdsFromEbmsCore) {
                if (!cpaIdsFromCPAService.contains(cpaId)) {
                    ebmsCoreCpaService.deleteCPA(cpaId);
                    LOGGER.info("Removed unknown CPA {} from EbMS Core.", cpaId);
                }
            }
            for (String cpaId : cpaIdsFromCPAService) {
                if (!cpaIdsFromEbmsCore.contains(cpaId)) {
                    String cpaString = getCPA(cpaId);
                    ebmsCoreCpaService.insertCPA(cpaString);
                    LOGGER.info("Added missing CPA {} to EbMS Core.", cpaId);
                }
            }
        }
        LOGGER.info("Done syncing CPAs");
    }

    public String getCPA(String cpaId) {
        return getCPAFromRepository(cpaId).getCpaContent();
    }

    public List<CPAParty> getCPAParties(String cpaId) {
        String cpaString = getCPA(cpaId);
        CollaborationProtocolAgreement cpa = CPAParser.parseToCollaborationProtocolAgreement(cpaString);
        return CPAParser.getPartiesFromCPA(cpa);
    }

    private CPA getCPAFromRepository(String cpaId) {
        Optional<CPA> cpa = cpaRepository.findByCpaId(cpaId);
        return cpa.orElseThrow(() -> new CPAServiceException(String.format("CPA with id %s not found", cpaId)));
    }

    public MultiValueMap<String, String> findCPA(MultiValueMap<String, String> requestParams) {
        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                String.valueOf(requestParams.getFirst("fromPartyId")),
                String.valueOf(requestParams.getFirst("toPartyId")),
                String.valueOf(requestParams.getFirst("action")),
                String.valueOf(requestParams.getFirst("service")),
                String.valueOf(requestParams.getFirst("serviceExtension")));
        LOGGER.debug("CPA Search Criteria: {} ", searchCriteria);
        return cpaSelectionService.findCPA(searchCriteria);
    }

    @Transactional
    @CacheEvict(cacheNames = "CPA_CACHE", allEntries = true)
    public void deleteCPA(String cpaId) {
        getCPAFromRepository(cpaId); // check if CPA exists
        cpaRepository.deleteByCpaId(cpaId);
        ebmsCoreCpaService.deleteCPA(cpaId); // necessary call until ebms core uses cpa-service-database
        LOGGER.info("Deleted CPA with id {}", cpaId);
    }

    @Transactional
    @CacheEvict(cacheNames = "CPA_CACHE", allEntries = true)
    public void activateCPA(String cpaId) {
        getCPAFromRepository(cpaId); // check if CPA exists
        cpaRepository.setActive(cpaId);
        LOGGER.info("Activated CPA with id {}", cpaId);
    }

    @Transactional
    @CacheEvict(cacheNames = "CPA_CACHE", allEntries = true)
    public void deactivateCPA(String cpaId) {
        getCPAFromRepository(cpaId); // check if CPA exists
        cpaRepository.setInActive(cpaId);
        LOGGER.info("Deactivated CPA with id {}", cpaId);
    }

    public void resendFailed(String cpaId) {
        jmsConsumerService.resendFailed(cpaId);
    }

    public void resendExpired(String cpaId) {
        jmsConsumerService.resendExpired(cpaId);
    }

}
