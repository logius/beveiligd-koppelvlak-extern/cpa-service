package nl.logius.osdbk.cpa.util;

import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import nl.logius.osdbk.cpa.exception.CPAServiceException;
import nl.logius.osdbk.cpa.model.CPA;
import nl.logius.osdbk.cpa.model.CPAParty;
import nl.logius.osdbk.cpa.model.CPARole;
import org.oasis.open.ebxml.cpa.CanReceive;
import org.oasis.open.ebxml.cpa.CanSend;
import org.oasis.open.ebxml.cpa.CollaborationRole;
import org.oasis.open.ebxml.cpa.PartyInfo;

public class CPAParser {

    private CPAParser() {
    }

    public static CollaborationProtocolAgreement parseToCollaborationProtocolAgreement(String cpaString) {
        try {
            JAXBContext jc = JAXBContext.newInstance(CollaborationProtocolAgreement.class);
            StringReader reader = new StringReader(cpaString);

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            return (CollaborationProtocolAgreement) unmarshaller.unmarshal(reader);
        } catch (JAXBException ex) {
            throw new CPAServiceException("CPA string can not be parsed. Invalid CPA. ", ex);
        }
    }

    public static CPA parseToCPAEntity(String cpaString) {
        CollaborationProtocolAgreement cpa = CPAParser.parseToCollaborationProtocolAgreement(cpaString);
        CPA cpaEntity = new CPA();
        cpaEntity.setCpaId(cpa.getCpaid());
        cpaEntity.setCpaContent(cpaString);
        cpaEntity.setVersion(cpa.getVersion());
        cpaEntity.setStartDate(cpa.getStart().toGregorianCalendar().getTime());
        cpaEntity.setEndDate(cpa.getEnd().toGregorianCalendar().getTime());
        cpaEntity.setInsertDate(new Date());
        cpaEntity.setIsActive(true);
        return cpaEntity;
    }

    public static List<CPAParty> getPartiesFromCPA(CollaborationProtocolAgreement cpa) {
        List<CPAParty> parties = new ArrayList<>();
        cpa.getPartyInfo().forEach(p -> {
            CPAParty party = new CPAParty();
            party.setPartyId(p.getPartyId().get(0).getValue());
            party.setPartyName(p.getPartyName());

            List<CPARole> roles = addRolesToCPAParty(p);
            party.setCpaRoles(roles);
            parties.add(party);
        });
        return parties;
    }

    private static List<CPARole> addRolesToCPAParty(PartyInfo p) {
        List<CPARole> roles = new ArrayList<>();
        for (CollaborationRole c : p.getCollaborationRole()) {
            CPARole role = new CPARole();
            role.setName(c.getRole().getName());

            List<String> receiveActions = new ArrayList<>();
            for (CanReceive cr : c.getServiceBinding().getCanReceive()) {
                if (!cr.getThisPartyActionBinding().getAction().equalsIgnoreCase("appTest")
                        && !cr.getThisPartyActionBinding().getAction().equalsIgnoreCase("appTestResponse")) {
                    receiveActions.add(cr.getThisPartyActionBinding().getAction());
                }
            }

            List<String> sendActions = new ArrayList<>();
            for (CanSend cs : c.getServiceBinding().getCanSend()) {
                if (!cs.getThisPartyActionBinding().getAction().equalsIgnoreCase("appTest")
                        && !cs.getThisPartyActionBinding().getAction().equalsIgnoreCase("appTestResponse")) {
                    sendActions.add(cs.getThisPartyActionBinding().getAction());
                }
            }

            role.setService(c.getServiceBinding().getService().getValue());
            role.setReceiveActions(receiveActions);
            role.setSendActions(sendActions);
            roles.add(role);
        }
        return roles;
    }

}
