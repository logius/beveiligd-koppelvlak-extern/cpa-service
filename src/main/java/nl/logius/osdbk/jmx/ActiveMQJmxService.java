package nl.logius.osdbk.jmx;

import java.io.IOException;
import java.util.HashMap;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import nl.logius.osdbk.cpa.exception.CPAServiceException;
import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.apache.activemq.broker.jmx.QueueViewMBean;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

@Service
public class ActiveMQJmxService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ActiveMQJmxService.class);
    private static final String PAUSE_ACTION = "pause";
    private static final String RESUME_ACTION = "resume";

    @Value("${spring.activemq.jmx-url}")
    private String jmxServiceUrl;
    @Value("${spring.activemq.broker-name}")
    private String activemqBrokername;
    @Value("${spring.activemq.user}")
    private String activemqUsername;
    @Value("${spring.activemq.password}")
    private String activemqPassword;

    public void toggleQueueStatus(String queueName, String action) {
        if (!action.equalsIgnoreCase(PAUSE_ACTION) && !action.equalsIgnoreCase(RESUME_ACTION)) {
            throw new CPAServiceException("Action Parameter should match either " + PAUSE_ACTION + " or " + RESUME_ACTION + ", but was " + action + ". Request can not be processed.");
        }

        try {
            MBeanServerConnection mBeanConnection = setupJmxConnection();
            ObjectName activeMQInstance = new ObjectName("org.apache.activemq:type=Broker,brokerName=" + activemqBrokername);
            BrokerViewMBean brokerMBean = MBeanServerInvocationHandler.newProxyInstance(mBeanConnection, activeMQInstance, BrokerViewMBean.class, true);

            for (ObjectName activemqQueueName : brokerMBean.getQueues()) {
                QueueViewMBean queueMbean = MBeanServerInvocationHandler.newProxyInstance(mBeanConnection, activemqQueueName, QueueViewMBean.class, true);
                if (queueMbean.getName().equals(queueName)) {
                    if (action.equalsIgnoreCase(PAUSE_ACTION)) {
                        queueMbean.pause();
                        LOGGER.info("Paused queue {}.", queueName);
                    } else if (action.equalsIgnoreCase(RESUME_ACTION)) {
                        queueMbean.resume();
                        LOGGER.info("Resumed queue {}.", queueName);
                    } else {
                        LOGGER.info("Queue with name {} not found. No queue operation executed.", queueName);

                    }
                }
            }
        } catch (IOException | MalformedObjectNameException ex) {
            throw new CPAServiceException("Something went wrong when connecting with ActiveMQ via JMX", ex);
        }
    }

    private MBeanServerConnection setupJmxConnection() throws IOException {
        HashMap<String, String[]> env = new HashMap<>();
        String[] adminCredentials = new String[]{activemqUsername, activemqPassword};
        env.put("jmx.remote.credentials", adminCredentials);

        JMXServiceURL jmxUrl = new JMXServiceURL(jmxServiceUrl);
        JMXConnector jmxConnector = JMXConnectorFactory.connect(jmxUrl, env);
        return jmxConnector.getMBeanServerConnection();

    }

}
