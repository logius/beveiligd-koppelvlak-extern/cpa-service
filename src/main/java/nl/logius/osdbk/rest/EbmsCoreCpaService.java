package nl.logius.osdbk.rest;

import lombok.RequiredArgsConstructor;
import nl.logius.osdbk.cpa.exception.CPAServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RequiredArgsConstructor
@Service
public class EbmsCoreCpaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EbmsCoreCpaService.class);

    private final RestTemplate restTemplate;

    @Value("${ebms.cpa-service.url}")
    private String ebmsCoreCpaServiceUrl;
    @Value("${ebms.cpa-service.username}")
    private String ebmsCoreCpaServiceUser;
    @Value("${ebms.cpa-service.password}")
    private String ebmsCoreCpaServicePassword;

    public void validateCPA(String cpa) {
        try {
            HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsCoreCpaServiceUser, ebmsCoreCpaServicePassword);
            HttpEntity<String> request = new HttpEntity<>(cpa, headers);
            restTemplate.exchange(ebmsCoreCpaServiceUrl + "/validate", HttpMethod.POST, request, Void.class);
        } catch (HttpServerErrorException ex) {
            if (ex.getRawStatusCode() == 500) {
                throw new CPAServiceException(String.format("CPA is invalid. Error is: %s", ex.getMessage()));
            } else {
                throw ex;
            }
        }
    }

    public void insertCPA(String cpaString) {
        try {
            HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsCoreCpaServiceUser, ebmsCoreCpaServicePassword);
            HttpEntity<String> request = new HttpEntity<>(cpaString, headers);
            restTemplate.exchange(ebmsCoreCpaServiceUrl + "?overwrite=true", HttpMethod.POST, request, String.class);
        } catch (HttpServerErrorException ex) {
            if (ex.getRawStatusCode() == 500) {
                throw new CPAServiceException(String.format("CPA is invalid. Error is: %s", ex.getMessage()));
            } else {
                throw ex;
            }
        }
    }

    public void deleteCPA(String cpaId) {
        try {
            HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsCoreCpaServiceUser, ebmsCoreCpaServicePassword);
            headers.add("Content-Type", "application/json");
            HttpEntity<?> request = new HttpEntity<>(headers);
            restTemplate.exchange(ebmsCoreCpaServiceUrl + "/" + cpaId, HttpMethod.DELETE, request, Void.class);
        } catch (HttpServerErrorException ex) {
            if (ex.getRawStatusCode() == 404) {
                LOGGER.info("CPA with id {} not found in EbMS Core", cpaId);
            } else {
                throw ex;
            }
        }
    }

    public List<String> getCpaIds() {
        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsCoreCpaServiceUser, ebmsCoreCpaServicePassword);
        HttpEntity<?> request = new HttpEntity<>(headers);
        ResponseEntity<List> response = restTemplate.exchange(ebmsCoreCpaServiceUrl, HttpMethod.GET, request, List.class);
        return response.getBody();
    }

}
