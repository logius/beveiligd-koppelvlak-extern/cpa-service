package nl.logius.osdbk.rest;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Service
public class EbmsCoreRestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EbmsCoreRestService.class);

    private final RestTemplate restTemplate;

    @Value("${ebms.rest-service.url}")
    private String ebmsCoreRestServiceUrl;
    @Value("${ebms.rest-service.username}")
    private String ebmsCoreRestServiceUser;
    @Value("${ebms.rest-service.password}")
    private String ebmsCoreRestServicePassword;

    public void ping(String cpaId, String fromPartyId, String toPartyId) {
        try {
            HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsCoreRestServiceUser, ebmsCoreRestServicePassword);
            headers.add("Content-Type", "application/json");
            HttpEntity<?> request = new HttpEntity<>(headers);
            restTemplate.exchange(ebmsCoreRestServiceUrl + "/ping/" + cpaId + "/from/" + fromPartyId + "/to/" + toPartyId, HttpMethod.POST, request, Void.class);
        } catch (HttpServerErrorException ex) {
            if (ex.getRawStatusCode() == 404) {
                LOGGER.info("CPA with id {} not found to Ping in EbMS Core", cpaId);
            } else {
                throw ex;
            }
        }
    }

}
