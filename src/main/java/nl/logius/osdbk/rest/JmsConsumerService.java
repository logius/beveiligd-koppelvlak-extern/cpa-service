package nl.logius.osdbk.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Service
public class JmsConsumerService {

    private final RestTemplate restTemplate;

    @Value("${ebms.resend.url}")
    private String resendUrl;
    @Value("${ebms.resend.username}")
    private String resendUser;
    @Value("${ebms.resend.password}")
    private String resendPassword;

    public void resendFailed(String cpaId) {
        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(resendUser, resendPassword);
        HttpEntity<Void> request = new HttpEntity<>(headers);
        restTemplate.put(resendUrl + "/failed/" + cpaId, request);
    }

    public void resendExpired(String cpaId) {
        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(resendUser, resendPassword);
        HttpEntity<Void> request = new HttpEntity<>(headers);
        restTemplate.put(resendUrl + "/expired/" + cpaId, request);
    }

}
