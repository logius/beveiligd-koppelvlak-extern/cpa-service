package nl.logius.osdbk;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import static org.mockito.Mockito.mockStatic;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootTest
class CPAServiceApplicationTests {

	@Test
    void testApplicationLoads() {

        try (MockedStatic<SpringApplication> mocked = mockStatic(SpringApplication.class)) {
            mocked.when(() -> SpringApplication.run(CPAServiceApplication.class, "foo", "bar"))
                    .thenReturn(Mockito.mock(ConfigurableApplicationContext.class));

            CPAServiceApplication.main(new String[]{"foo", "bar"});

            mocked.verify(() -> SpringApplication.run(CPAServiceApplication.class, "foo", "bar"));
        }
    }

}
