package nl.logius.osdbk.controller;

import nl.logius.osdbk.cpa.orchestrator.CPAAndEbmsCoreRestOrchestrator;
import nl.logius.osdbk.cpa.service.CPAService;
import nl.logius.osdbk.jmx.ActiveMQJmxService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@SpringBootTest
class CPAServiceControllerTest {

    @InjectMocks
    private CPAServiceController controller;
    @Mock
    private CPAAndEbmsCoreRestOrchestrator orchestratorService;
    @Mock
    private CPAService service;
    @Mock
    private ActiveMQJmxService jmxService;

    private final String cpaString = "content";
    private final String cpaId = "id";

    @BeforeEach
    void setUp() {
    }

    @Test
    void testValidateCPA() {
        controller.validateCPA(cpaString);
        verify(service, times(1)).validateCPA(cpaString);
    }

    @Test
    void testInsertCPA() {
        controller.insertCPA(cpaString);
        verify(service, times(1)).insertCPA(cpaString);
    }

    @Test
    void testGetCPAIds() {
        controller.getCPAIds();
        verify(service, times(1)).getCPAIds();
    }

    @Test
    void testGetActiveCPAIds() {
        controller.getActiveCPAIds();
        verify(service, times(1)).getActiveCPAIds();
    }

    @Test
    void testGetInactiveCPAIds() {
        controller.getInactiveCPAIds();
        verify(service, times(1)).getInactiveCPAIds();
    }

    @Test
    void testSynchronizeCPAs() {
        controller.synchronizeCPAs();
        verify(service, times(1)).synchronizeCPAs();
    }

    @Test
    void testGetCPA() {
        controller.getCPA(cpaId);
        verify(service, times(1)).getCPA(cpaId);
    }

    @Test
    void ping() {
        String cpaId = "someCpaId";
        int fromPartyIdx = 0;
        int toPartyIdx = 1;
        controller.ping(cpaId, fromPartyIdx, toPartyIdx);
        verify(orchestratorService, times(1)).ping(cpaId, fromPartyIdx, toPartyIdx);
    }

    @Test
    void testCPAParties() {
        controller.getCPAParties(cpaId);
        verify(service, times(1)).getCPAParties(cpaId);
    }

    @Test
    void testFindCPA() {
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        controller.findCPA(requestParams);
        verify(service, times(1)).findCPA(requestParams);
    }

    @Test
    void testDeleteCPA() {
        controller.deleteCPA(cpaId);
        verify(service, times(1)).deleteCPA(cpaId);
    }

    @Test
    void testActivateCPA() {
        controller.activateCPA(cpaId);
        verify(service, times(1)).activateCPA(cpaId);
    }

    @Test
    void testDeactivateCPA() {
        controller.deactivateCPA(cpaId);
        verify(service, times(1)).deactivateCPA(cpaId);
    }

    @Test
    void testResendFailed() {
        controller.resendFailedMessagesForCPA(cpaId);
        verify(service, times(1)).resendFailed(cpaId);
    }

    @Test
    void testResendExpired() {
        controller.resendExpiredMessagesForCPA(cpaId);
        verify(service, times(1)).resendExpired(cpaId);
    }

    @Test
    void testManipulateQueue() {
        controller.toggleQueueStatus("queue", "pause");
        verify(jmxService, times(1)).toggleQueueStatus("queue", "pause");
    }

}
