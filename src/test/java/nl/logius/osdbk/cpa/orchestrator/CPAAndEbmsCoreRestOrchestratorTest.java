package nl.logius.osdbk.cpa.orchestrator;

import nl.logius.osdbk.cpa.exception.CPAServiceException;
import nl.logius.osdbk.cpa.model.CPAParty;
import nl.logius.osdbk.cpa.service.CPAService;
import nl.logius.osdbk.rest.EbmsCoreRestService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
class CPAAndEbmsCoreRestOrchestratorTest {

    @InjectMocks
    private CPAAndEbmsCoreRestOrchestrator orchestrator;
    @Mock
    private CPAService cpaService;
    @Mock
    private EbmsCoreRestService ebmsCoreRestService;

    private static List<CPAParty> cpaParties;

    @BeforeAll
    static void setUp() {
        var cpaParty0 = new CPAParty();
        cpaParty0.setPartyId("partyId0");
        var cpaParty1 = new CPAParty();
        cpaParty1.setPartyId("partyId1");
        cpaParties = List.of(cpaParty0, cpaParty1);
    }

    @Test
    void pingWithValidPartyIds0to1() {
        //when calling getCPAParties on cpaService, returns cpaParties
        when(cpaService.getCPAParties(anyString())).thenReturn(cpaParties);
        //when calling ping on ebmsCoreRestService, returns nothing
        orchestrator.ping("someCpaId", 0, 1);
        verify(cpaService, times(1)).getCPAParties(anyString());
        verify(ebmsCoreRestService, times(1)).ping(anyString(), anyString(), anyString());
    }

    @Test
    void pingWithInvalidPartyIds0to0() {
        //when calling getCPAParties on cpaService, returns cpaParties
        when(cpaService.getCPAParties(anyString())).thenReturn(cpaParties);
        //when calling ping on ebmsCoreRestService with same indexes used, expect an CPAServiceException
        assertThrows(CPAServiceException.class, () -> orchestrator.ping("someCpaId", 0, 0));
        verify(cpaService, times(0)).getCPAParties(anyString());
        verify(ebmsCoreRestService, times(0)).ping(anyString(), anyString(), anyString());
    }

    @Test
    void pingWithInvalidPartyIds() {
        //when calling getCPAParties on cpaService, returns cpaParties
        when(cpaService.getCPAParties(anyString())).thenReturn(cpaParties);
        //when calling ping on ebmsCoreRestService with invalid indexes used, expect an IndexOutOfBoundsException
        assertThrows(IndexOutOfBoundsException.class, () -> orchestrator.ping("someCpaId", -1, 0));
        verify(cpaService, times(1)).getCPAParties(anyString());
        verify(ebmsCoreRestService, times(0)).ping(anyString(), anyString(), anyString());
    }
}