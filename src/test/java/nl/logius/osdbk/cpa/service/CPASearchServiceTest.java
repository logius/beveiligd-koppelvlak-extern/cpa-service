package nl.logius.osdbk.cpa.service;

import nl.logius.osdbk.cpa.util.CPAParser;
import nl.logius.osdbk.cpa.util.CPASearchCriteria;
import nl.logius.osdbk.cpa.exception.CPASelectionException;
import org.assertj.core.util.Files;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import nl.logius.osdbk.cpa.model.CPA;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import org.springframework.util.MultiValueMap;
import nl.logius.osdbk.cpa.repository.CPARepository;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class CPASearchServiceTest {

    @Mock
    private CPARepository cpaRepository;
    @InjectMocks
    private CPASelectionService searchService;

    private CPA validCPA;
    private final String cpaId = "DGL-ONTVANGEN-1-0$1-0_22222222222222222222-00000004003214345001_D8B5DA3079F311E8B020005056810F3E";
    private String cpaString;

    @BeforeEach
    public void setup() throws IOException {
        File cpaFile = ResourceUtils.getFile("classpath:valid_cpa.xml");
        cpaString = Files.contentOf(cpaFile, StandardCharsets.UTF_8);
        validCPA = createValidCPA(cpaString);
    }

    @Test
    void findCPA() {
        when(cpaRepository.findActiveCpas()).thenReturn(Collections.singletonList(validCPA));

        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");

        MultiValueMap<String, String> responseParams = searchService.findCPA(searchCriteria);
        assertEquals(validCPA.getCpaId(), responseParams.getFirst("cpaId"));
        assertEquals(validCPA.getCpaContent(), cpaString);
        assertEquals("1.0", validCPA.getVersion());
        assertTrue(validCPA.isIsActive());
        assertEquals(validCPA.toString(), "nl.logius.osdbk.cpa.Cpa[ cpaId=" + cpaId + " ]");
        assertNotNull(validCPA.getStartDate());
        assertNotNull(validCPA.getEndDate());
        assertNotNull(validCPA.getInsertDate());
        
    }

    @Test
    void findCPANotFound() {
        when(cpaRepository.findActiveCpas()).thenReturn(new ArrayList<>());

        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");

        assertThrows(CPASelectionException.class, () -> searchService.findCPA(searchCriteria));
    }

    @Test
    void findCPAMultipleResults() {
        when(cpaRepository.findActiveCpas()).thenReturn(Arrays.asList(validCPA, validCPA));

        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");

        CPASelectionException thrown = assertThrows(CPASelectionException.class, () -> searchService.findCPA(searchCriteria));

        String errorMessageShouldBe = "Error. Multiple CPA's found for message with sender="
                + searchCriteria.getSender() + ", receiver=" + searchCriteria.getReceiver() + ", action=" + searchCriteria.getAction()
                + " and service=" + searchCriteria.getService() + ". CPAs: " + cpaId + ", " + cpaId + ", ";
        assertEquals(thrown.getMessage(), errorMessageShouldBe);
        // test for sonar score
        assertEquals(searchCriteria.toString(), "CPASearchCriteria: 00000004003214345001 - 22222222222222222222 - verstrekkingAanAfnemer - dgl:ontvangen:1.0 - $1.0");
    }
    
    private CPA createValidCPA(String cpaString) {
        return CPAParser.parseToCPAEntity(cpaString);
    }

}
