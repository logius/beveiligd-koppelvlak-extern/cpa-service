package nl.logius.osdbk.cpa.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import nl.logius.osdbk.cpa.model.CPA;
import nl.logius.osdbk.cpa.model.CPAParty;
import nl.logius.osdbk.cpa.repository.CPARepository;
import nl.logius.osdbk.cpa.util.CPASearchCriteria;
import nl.logius.osdbk.rest.EbmsCoreCpaService;
import nl.logius.osdbk.rest.JmsConsumerService;
import org.assertj.core.util.Files;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;

@ExtendWith(MockitoExtension.class)
class CPAServiceTest {

    @InjectMocks
    private CPAService cpaService;
    @Mock
    private CPARepository cpaRepository;
    @Mock
    private CPASelectionService cpaSearchService;
    @Mock
    private EbmsCoreCpaService ebmsCoreCpaService;
    @Mock
    private JmsConsumerService jmsConsumerService;

    private String cpaString;

    @BeforeEach
    void setUp() throws FileNotFoundException {
        File cpaFile = ResourceUtils.getFile("classpath:valid_cpa.xml");
        cpaString = Files.contentOf(cpaFile, StandardCharsets.UTF_8);
    }

    @Test
    void testValidateCPA() {
        cpaService.validateCPA(cpaString);
        verify(ebmsCoreCpaService, times(1)).validateCPA(cpaString);
    }

    @Test
    void testInsertCPA() {
        String cpaId = cpaService.insertCPA(cpaString);
        
        // compare to the cpaId in the valid_cpa.xml file
        verify(ebmsCoreCpaService, times(1)).insertCPA(cpaString);
        assertEquals("DGL-ONTVANGEN-1-0$1-0_22222222222222222222-00000004003214345001_D8B5DA3079F311E8B020005056810F3E", cpaId);
    }

    @Test
    void testGetCPAIds() {
        when(cpaRepository.findCpaIds()).thenReturn(Arrays.asList("1", "2"));
        List<String> cpaIds = cpaService.getCPAIds();
        assertEquals(2, cpaIds.size());
    }

    @Test
    void testGetActiveCPAIds() {
        when(cpaRepository.findActiveCpaIds()).thenReturn(Arrays.asList("1", "2"));
        List<String> cpaIds = cpaService.getActiveCPAIds();
        assertEquals(2, cpaIds.size());
    }

    @Test
    void testGetInactiveCPAIds() {
        when(cpaRepository.findInactiveCpaIds()).thenReturn(Arrays.asList("1", "2"));
        List<String> cpaIds = cpaService.getInactiveCPAIds();
        assertEquals(2, cpaIds.size());
    }

    @Test
    void testSynchronizeCPAsNoDifference() {
        when(ebmsCoreCpaService.getCpaIds()).thenReturn(Arrays.asList("1", "2"));
        when(cpaRepository.findCpaIds()).thenReturn(Arrays.asList("1", "2"));
        
        cpaService.synchronizeCPAs();
        
        verify(ebmsCoreCpaService, times(0)).insertCPA(anyString());
        verify(ebmsCoreCpaService, times(0)).deleteCPA(anyString());
    }

    @Test
    void testSynchronizeCPAsMissingCPAInCore() {
        when(ebmsCoreCpaService.getCpaIds()).thenReturn(List.of("1"));
        when(cpaRepository.findCpaIds()).thenReturn(Arrays.asList("1", "2"));
        
        CPA cpaMock = Mockito.mock(CPA.class);
        when(cpaMock.getCpaContent()).thenReturn("cpaContent");
        when(cpaRepository.findByCpaId(anyString())).thenReturn(Optional.of(cpaMock));
        
        cpaService.synchronizeCPAs();
        
        verify(ebmsCoreCpaService, times(1)).insertCPA(anyString());
        verify(ebmsCoreCpaService, times(0)).deleteCPA(anyString());
    }

    @Test
    void testSynchronizeCPAsExtraCPAInCore() {
        when(ebmsCoreCpaService.getCpaIds()).thenReturn(List.of("1", "2"));
        when(cpaRepository.findCpaIds()).thenReturn(Arrays.asList("1"));
        
        cpaService.synchronizeCPAs();
        
        verify(ebmsCoreCpaService, times(0)).insertCPA(anyString());
        verify(ebmsCoreCpaService, times(1)).deleteCPA(anyString());
    }

    @Test
    void testGetCPA() {
        CPA cpaMock = Mockito.mock(CPA.class);
        when(cpaMock.getCpaContent()).thenReturn("cpaContent");
        when(cpaRepository.findByCpaId(anyString())).thenReturn(Optional.of(cpaMock));
        
        String content = cpaService.getCPA("1");
        assertEquals("cpaContent", content);
    }

    @Test
    void testGetCPAParties() {
        CPA cpaMock = Mockito.mock(CPA.class);
        CPAParty cpaPartyMock = Mockito.mock(CPAParty.class);
        when(cpaRepository.findByCpaId(anyString())).thenReturn(Optional.of(cpaMock));
        when(cpaMock.getCpaContent()).thenReturn(cpaString);
        
        List<CPAParty> parties = cpaService.getCPAParties("1");
        assertEquals(2, parties.size());
    }

    @Test
    void testFindCPA() {
        when(cpaSearchService.findCPA(any(CPASearchCriteria.class))).thenReturn(new LinkedMultiValueMap<>());
        
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        cpaService.findCPA(requestParams);
        verify(cpaSearchService, times(1)).findCPA(any(CPASearchCriteria.class));
    }

    @Test
    void testDeleteCPA() {
        CPA cpaMock = Mockito.mock(CPA.class);
        when(cpaRepository.findByCpaId(anyString())).thenReturn(Optional.of(cpaMock));
        cpaService.deleteCPA("1");
        verify(ebmsCoreCpaService, times(1)).deleteCPA(anyString());
        verify(cpaRepository, times(1)).deleteByCpaId(anyString());
    }

    @Test
    void testActivateCPA() {
        CPA cpaMock = Mockito.mock(CPA.class);
        when(cpaRepository.findByCpaId(anyString())).thenReturn(Optional.of(cpaMock));
        cpaService.activateCPA("1");
        verify(cpaRepository, times(1)).setActive(anyString());
    }

    @Test
    void testDeactivateCPA() {
        CPA cpaMock = Mockito.mock(CPA.class);
        when(cpaRepository.findByCpaId(anyString())).thenReturn(Optional.of(cpaMock));
        cpaService.deactivateCPA("1");
        verify(cpaRepository, times(1)).setInActive(anyString());
    }

    @Test
    void testResendFailed() {
        cpaService.resendFailed("1");
        verify(jmsConsumerService, times(1)).resendFailed("1");
    }

    @Test
    void testResendExpired() {
        cpaService.resendExpired("1");
        verify(jmsConsumerService, times(1)).resendExpired("1");
    }


}
