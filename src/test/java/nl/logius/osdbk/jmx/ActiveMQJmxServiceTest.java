package nl.logius.osdbk.jmx;

import nl.logius.osdbk.cpa.exception.CPAServiceException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class ActiveMQJmxServiceTest {
    
    @InjectMocks
    ActiveMQJmxService jmxService;
    
    private static final String PAUSE_ACTION = "pause";
    private static final String RESUME_ACTION = "resume";
    
    private final String jmxServiceUrl = "service:jmx:rmi:///jndi/rmi://localhost:10051/jmxrmi";
    private final String activemqBrokername = "fakeBroker";
    private final String activemqUsername = "faker";
    private final String activemqPassword = "fake";
    
    
    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(jmxService, "jmxServiceUrl", jmxServiceUrl);
        ReflectionTestUtils.setField(jmxService, "activemqBrokername", activemqBrokername);
        ReflectionTestUtils.setField(jmxService, "activemqUsername", activemqUsername);
        ReflectionTestUtils.setField(jmxService, "activemqPassword", activemqPassword);
    }
    
    @Test
    void testToggleQueueWrongStatus() {
        
        CPAServiceException thrown = assertThrows(CPAServiceException.class, () -> jmxService.toggleQueueStatus("queue", "paused"));
        
        assertEquals(thrown.getMessage(), "Action Parameter should match either " + PAUSE_ACTION + " or " + RESUME_ACTION + ", but was paused. Request can not be processed.");
    }
    
}
