package nl.logius.osdbk.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class EbmsCoreCPAAndEbmsCoreRestOrchestratorTest {

    @InjectMocks
    private EbmsCoreRestService service;
    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    public void setup() {
        String url = "http://localhost:8080";
        ReflectionTestUtils.setField(service, "ebmsCoreRestServiceUrl", url);
        String username = "user";
        ReflectionTestUtils.setField(service, "ebmsCoreRestServiceUser", username);
        String password = "pw";
        ReflectionTestUtils.setField(service, "ebmsCoreRestServicePassword", password);
    }

    @Test
    void ping() {
        service.ping("someCpaId", "someFromPartyId", "someToPartyId");
        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }
}