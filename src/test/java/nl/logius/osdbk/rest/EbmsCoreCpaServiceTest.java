package nl.logius.osdbk.rest;

import java.util.Arrays;
import java.util.List;
import nl.logius.osdbk.cpa.exception.CPAServiceException;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class EbmsCoreCpaServiceTest {

    @InjectMocks
    private EbmsCoreCpaService service;
    @Mock
    private RestTemplate restTemplate;

    private final String url = "http://localhost:8080";
    private final String username = "user";
    private final String password = "pw";

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(service, "ebmsCoreCpaServiceUrl", url);
        ReflectionTestUtils.setField(service, "ebmsCoreCpaServiceUser", username);
        ReflectionTestUtils.setField(service, "ebmsCoreCpaServicePassword", password);
    }

    @Test
    public void testValidateCPA() {
        service.validateCPA("cpaString");
        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testValidateCPAInvalid() {
        HttpServerErrorException hseException = new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        doThrow(hseException).when(restTemplate).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        
        assertThrows(CPAServiceException.class,
                () -> {
                    service.validateCPA("cpaString");
                });

        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testValidateCPAError() {
        HttpServerErrorException hseException = new HttpServerErrorException(HttpStatus.BAD_REQUEST);
        doThrow(hseException).when(restTemplate).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        
        assertThrows(HttpServerErrorException.class,
                () -> {
                    service.validateCPA("cpaString");
                });

        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testInsertCPA() {
        service.insertCPA("cpaString");
        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testInsertCPAInvalid() {
        HttpServerErrorException hseException = new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        doThrow(hseException).when(restTemplate).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        
        assertThrows(CPAServiceException.class,
                () -> {
                    service.insertCPA("cpaString");
                });

        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testInsertCPAError() {
        HttpServerErrorException hseException = new HttpServerErrorException(HttpStatus.BAD_REQUEST);
        doThrow(hseException).when(restTemplate).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        
        assertThrows(HttpServerErrorException.class,
                () -> {
                    service.insertCPA("cpaString");
                });

        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testDeleteCPA() {
        service.deleteCPA("cpaId");
        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testDeleteCPANotFound() {
        HttpServerErrorException hseException = new HttpServerErrorException(HttpStatus.NOT_FOUND);
        doThrow(hseException).when(restTemplate).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        
        service.deleteCPA("cpaString");

        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

    @Test
    public void testGetCpaIds() {
        ResponseEntity<List> responseMock = mock(ResponseEntity.class);
        when(responseMock.getBody()).thenReturn(Arrays.asList("1", "2"));
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseMock);

        List<String> cpaIds = service.getCpaIds();
        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
    }

}
