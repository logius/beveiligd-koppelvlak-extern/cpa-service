package nl.logius.osdbk.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class JmsConsumerServiceTest {

    @InjectMocks
    private JmsConsumerService service;

    @Mock
    private RestTemplate restTemplate;

    private final String url = "http://localhost:8080/resend";
    private final String username = "user";
    private final String password = "pw";

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(service, "resendUrl", url);
        ReflectionTestUtils.setField(service, "resendUser", username);
        ReflectionTestUtils.setField(service, "resendPassword", password);
    }

    @Test
    public void testResendFailed() {
        service.resendFailed("cpaId");
        verify(restTemplate, times(1)).put(anyString(), any(HttpEntity.class));
    }

    @Test
    public void testResendExpired() {
        service.resendExpired("cpaId");
        verify(restTemplate, times(1)).put(anyString(), any(HttpEntity.class));
    }

}
