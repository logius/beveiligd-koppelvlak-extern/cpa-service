package nl.logius.osdbk.security;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SecurityConfigTest {

    @Test
    void isSecurityConfigBeanConfigured(ApplicationContext context) {
        assertThat(context.getBean(SecurityConfig.class)).isNotNull();
    }
}
